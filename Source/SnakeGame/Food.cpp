// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "FoodElementBase.h"
#include "FoodElementBonus.h"
#include "FoodElementBonusDead.h"
#include "PlayerPawnBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	FoodDestroy = 0;
	BonusFoodDestroy = 0;
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FoodDestroy += DeltaTime;
	if (FoodDestroy > FoodSpawnTimer)
	{
		SpawnFood();
		FoodDestroy = 0;
	}

	BonusFoodDestroy += DeltaTime;
	if (BonusFoodDestroy > BonusFoodSpawnTimer)
	{
		SpawnBonusFood();
		BonusFoodDestroy = 0;
	}

	DeadBonusFoodDestroy += DeltaTime;
	if (DeadBonusFoodDestroy > DeadBonusFoodSpawnTimer)
	{
		SpawnDeadBonusFood();
		DeadBonusFoodDestroy = 0;
	}
}

void AFood::SpawnFood()
{
	FRotator RotationPoint = FRotator(0, 0, 0);
	float X = FMath::FRandRange(MinX, MaxX);
	float Y = FMath::FRandRange(MinY, MaxY);

	FVector SpawnPoint = FVector(X, Y, Z);
	GetWorld()->SpawnActor<AFoodElementBase>(FoodBaseClass, SpawnPoint, RotationPoint);
}

void AFood::SpawnBonusFood()
{
	FRotator RotationPoint = FRotator(0, 0, 0);
	float X = FMath::FRandRange(MinX, MaxX);
	float Y = FMath::FRandRange(MinY, MaxY);

	FVector SpawnPoint = FVector(X, Y, Z);
	GetWorld()->SpawnActor<AFoodElementBonus>(FoodBonusClass, SpawnPoint, RotationPoint);

}

void AFood::SpawnDeadBonusFood()
{
	FRotator RotationPoint = FRotator(0, 0, 0);
	float X = FMath::FRandRange(MinX, MaxX);
	float Y = FMath::FRandRange(MinY, MaxY);

	FVector SpawnPoint = FVector(X, Y, Z);
	GetWorld()->SpawnActor<AFoodElementBonusDead>(FoodBonusDeadClass, SpawnPoint, RotationPoint);
}
