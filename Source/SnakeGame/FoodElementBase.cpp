// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodElementBase.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFoodElementBase::AFoodElementBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AFoodElementBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodElementBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FoodLifeTimer -= DeltaTime;
	if (FoodLifeTimer < FoodDestroy)
	{
		Destroy();
	}
}

void AFoodElementBase::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->Score++;
			Destroy();
		}
	}
}