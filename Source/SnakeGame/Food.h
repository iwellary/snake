// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Food.generated.h"

class AFoodElementBase;
class AFoodElementBonus;
class AFoodElementBonusDead;

UCLASS()
class SNAKEGAME_API AFood : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFoodElementBase> FoodBaseClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFoodElementBonus> FoodBonusClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFoodElementBonusDead> FoodBonusDeadClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "FoodPawn")
		void SpawnFood();

	UFUNCTION(BlueprintCallable, Category = "FoodPawn")
		void SpawnBonusFood();

	UFUNCTION(BlueprintCallable, Category = "FoodPawn")
		void SpawnDeadBonusFood();

	float MinX = -350.f;
	float MaxX = 350.f;
	float MinY = -350.f;
	float MaxY = 350.f;

	float Z = 0.f;

	float FoodSpawnTimer = 4.f;
	float FoodDestroy = 0;
	float BonusFoodSpawnTimer = 10.f;
	float BonusFoodDestroy = 0;
	float DeadBonusFoodSpawnTimer = 14.f;
	float DeadBonusFoodDestroy = 0;

};