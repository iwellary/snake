// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodElementBonus.h"
#include "SnakeBase.h"
#include "PlayerPawnBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFoodElementBonus::AFoodElementBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void AFoodElementBonus::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AFoodElementBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FoodLifeTimer -= DeltaTime;
	if (FoodLifeTimer < FoodDestroy)
	{
		Destroy();
	}
}

void AFoodElementBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Score += 5;
			Snake->SetActorTickInterval(0.1);
			Snake->BonusOff = 0;
			Destroy();
		}
	}
}